// number 3,4 and 5
let trainer = {
	name: 'Ash',
	age: 10,
	pokemon: ['Pikachu','Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoen: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log(`${this.pokemon[0]}! I choose you!`)
	}
}

console.log(trainer)

console.log(`Result of dot notation`)
console.log(trainer.name)

console.log(`Result of square bracket notation`)
console.log(trainer['pokemon'])

console.log(`Result of talk method`)
trainer.talk()


// number 8
function pokemonNames(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = attack;
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)
		let health = target.health - this.attack
		if(health <= 0){
			console.log(`${target.name} fainted`)
		}
	};
}

let pikachu = new pokemonNames('Pikachu', 12, 24, 12)
console.log(pikachu)
let geodude = new pokemonNames('Geodude', 8, 16, 8)
console.log(geodude)
let mewtwo = new pokemonNames('Mewtwo', 200, 200, 100)
console.log(mewtwo)

geodude.tackle(pikachu)
mewtwo.tackle(geodude)


